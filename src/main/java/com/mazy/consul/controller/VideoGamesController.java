package com.mazy.consul.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("")
@Api(tags = "default")
public class VideoGamesController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * <p>
	 * Metodo para retornar la lista de VideoJuegos de la BD
	 * </p>
	 * 
	 * @author Alonso
	 * @param void
	 * @return La lista de Videojuegos
	 */
	@ApiOperation(value = "Get all VideoGames", notes = "Get all videogames")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/getVideoGames", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<String> listPets() {
		log.info("GetMapping - value: /getVideoGames");

		// dummy
		List<String> lstVideoGames = new ArrayList<String>();
		lstVideoGames.add("God of War");
		lstVideoGames.add("Spiderman");
		return lstVideoGames;
	}

	@ApiOperation(value = "Get all VideoGames", notes = "Get all videogames")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public String health() {
		log.info("GetMapping - value: /getVideoGames");
		return "ok";
	}
	
}
