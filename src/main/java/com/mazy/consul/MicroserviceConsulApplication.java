package com.mazy.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceConsulApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceConsulApplication.class, args);
	}

}
